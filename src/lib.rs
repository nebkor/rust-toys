fn strtok<'s>(s: &mut &'s str, d: char) -> &'s str {
    if let Some(loc) = s.find(d) {
        let prefix = &s[..loc];
        let suffix = &s[loc + 1..];
        *s = suffix;
        prefix
    } else {
        let prefix = &s[..];
        *s = "";
        prefix
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_toks() {
        let mut s = "hello world";
        let hello = strtok(&mut s, 'o');

        assert_eq!(hello, "hell");
        assert_eq!(s, " world");

        let world = strtok(&mut s, '_');
        assert_eq!(" world", world);
        assert_eq!(s, "");
    }
}
