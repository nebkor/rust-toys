#![feature(core_intrinsics)]

use std::any::Any;

fn get_type_of<T: ?Sized + Any>(_s: &T) -> &str {
    unsafe { std::intrinsics::type_name::<T>() }
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Butt(Vec<usize>);

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Fart(Vec<usize>);

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Snot(Vec<usize>);

impl Butt {
    pub fn new() -> Self {
        Butt(Vec::new())
    }

    pub fn resize(&mut self, target_len: usize) {
        if self.len() < target_len {
            self.0.reserve(target_len);
            while self.len() < target_len {
                let sz = self.len();
                self.0.push(sz);
            }
        } else {
            self.0.resize(target_len, 0);
        }
    }

    pub fn reset(&mut self, target_len: usize) {
        self.0 = (0..target_len).collect();
    }
}

impl Fart {
    pub fn new() -> Self {
        Fart(Vec::new())
    }
}

impl Snot {
    pub fn new() -> Self {
        Snot(Vec::new())
    }
}

impl std::ops::Deref for Butt {
    type Target = [usize];
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl std::ops::Deref for Fart {
    type Target = Vec<usize>;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl std::ops::DerefMut for Fart {
    fn deref_mut(&mut self) -> &mut Vec<usize> {
        self.0.as_mut()
    }
}

// Not possible?:
/* impl std::ops::Deref for Snot {
    type Target = Butt;
    fn deref(&self) -> &Self::Target {
        &Butt { self.0 }
    }
} */

// Snots can turn into Butts
impl std::convert::From<Snot> for Butt {
    fn from(s: Snot) -> Self {
        Butt(s.0)
    }
}

fn main() {
    let mut butt = Butt::new();
    let mut fart = Fart::new();
    let snot = Snot::new();

    println!("butt is a {}", get_type_of(&butt));
    println!("*butt is a {}", get_type_of(&(*butt)));
    // self.0 = '1'
    butt.resize(1);
    assert!(butt[0] == 0);
    assert!(butt.len() == 1);

    println!("fart is a {}", get_type_of(&fart));
    println!("*fart is a {}", get_type_of(&(*fart)));
    // self.0 = '2'
    fart.push(2);
    assert!(fart[0] == 2);
    assert!(fart.len() == 1);

    println!("snot is a {}", get_type_of(&snot));
    // self.len == 3
    let mut sb = Butt::from(snot);
    println!("sb is a {}", get_type_of(&sb));
    sb.resize(3);
    assert!(sb.len() == 3);
    assert!(sb[2] == 2);

    println!("sb is: {:?}", sb);
}
